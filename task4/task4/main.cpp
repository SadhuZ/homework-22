#include <iostream>
#include <fstream>
#include <vector>
using namespace std;

string str_tolower(string s) {
    string str;
    for(int i = 0; i < s.size(); i++)
        str += tolower(s[i]);
    return str;
}

int main()
{
    string filePath;
    cout << "Enter a file path: ";
    cin >> filePath;
    if(str_tolower(filePath.substr(filePath.size() - 4, 4)) != ".png" ){
        cout << "Wrong file type name." << endl;
        return -1;
    }

    ifstream pngFile;
    pngFile.open(filePath, ios::binary);
    if(!pngFile.is_open()){
        cout << "File open error." << endl;
        return -1;
    }

    char byte[4];    
    pngFile.read(byte, 4);
    if(byte[0] == -119 && byte[1] == 'P' && byte[2] == 'N' && byte[3] == 'G')
        cout << "PNG file." << endl;
    else
        cout << "Not PNG file." << endl;
    pngFile.close();
    return 0;
}
