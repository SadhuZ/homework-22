#include <iostream>
#include <fstream>

using namespace std;

int main()
{
    const char* fileName = "vedomost.txt";
    ifstream vedomost;
    vedomost.open(fileName);
    if(!vedomost.is_open()){
        cout << "File open error." << endl;
        return -1;
    }
    string maxPayPerson;
    int sumPay = 0;
    int maxPay = 0;
    while (!vedomost.eof()) {
        string name;
        string surename;
        int payment;
        string date;
        vedomost >> name >> surename >> payment >> date;
        sumPay += payment;
        if(maxPay < payment){
            maxPay = payment;
            maxPayPerson = name + " " + surename;
        }
    }
    vedomost.close();
    cout << "Amount of payments: " << sumPay << "." << endl;
    cout << maxPayPerson << " has a maximum income." << endl;
    return 0;
}
