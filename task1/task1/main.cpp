#include <iostream>
#include <fstream>
#include <vector>
using namespace std;

int main()
{
    const char* fileName = "Eyes Do More Than See.txt";
    ifstream text;
    text.open(fileName);
    if(!text.is_open()){
        cout << "File open error." << endl;
        return -1;
    }
    string searchWord;
    cout << "Enter a search word: ";
    cin >> searchWord;
    int count = 0;
    while (!text.eof()) {
        string word;
        text >> word;
        if(searchWord == word)
            count++;
    }
    cout << "The word \"" << searchWord << "\" was found " << count << " times." << endl;
    text.close();
    return 0;
}
