#include <iostream>
#include <fstream>

using namespace std;

int ReadQuestion(int questionNum){
    string questionFileName = "..//questions//" + to_string(questionNum + 1) + ".txt";
    ifstream questionFile;
    questionFile.open(questionFileName, ios::binary);
    if(!questionFile.is_open()){
        cout << "Question file open error." << endl;
        return 0;
    }
    cout << "Question is:" << endl;
    while (!questionFile.eof()) {
        char byte;
        questionFile.read(&byte, 1);
        cout << byte;
    }
    questionFile.close();
    return 1;
}

int GetAnswer(int questionNum){
    string answerFileName = "..//answers//" + to_string(questionNum + 1) + ".txt";
    ifstream answerFile;
    answerFile.open(answerFileName, ios::binary);
    if(!answerFile.is_open()){
        cout << "Answer file open error." << endl;
        return 0;
    }
    string answer;
    string rightAnswer;
    cout << "Enter your answer: " << endl;
    cin >> answer;
    answerFile >> rightAnswer;
    if(answer == rightAnswer){
        cout << "Right answer!" << endl;
        return 1;
    }
    else{
        cout << "Wrong answer!" << endl;
        return 2;
    }
    answerFile.close();
}

int main()
{
    bool enableSector[13] = {1,1,1,1,1,1,1,1,1,1,1,1,1};
    int currentSector = 0;
    int offset;
    int gamerScore = 0;
    int computerScore = 0;

    cout << "Current sector: " << currentSector + 1 << endl;
    while (gamerScore < 6 && computerScore < 6) {
        cout << "Enter offset: ";
        cin >> offset;

        while(enableSector[(currentSector + offset) % 13] == false)
            offset++;
        currentSector = (currentSector + offset) % 13;
        enableSector[currentSector] = false;
        cout << "Current sector: " << currentSector + 1 << endl;
        if(ReadQuestion(currentSector)){
            int win = GetAnswer(currentSector);
            if(win == 1)
                gamerScore++;
            else if(win == 2)
                computerScore++;
            else
                return -1;
        }
        cout << "Score - " << gamerScore << " : " << computerScore << endl;
    }
    if(gamerScore == 6)
        cout << "You win!" << endl;
    else
        cout << "Computer win!" << endl;
    return 0;
}
