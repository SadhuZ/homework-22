#include <iostream>
#include <fstream>
#include <vector>
using namespace std;

int main()
{
    string fileName;
    cout << "Enter a file name: ";
    cin >> fileName;

    ifstream text;
    text.open(fileName, ios::binary);
    if(!text.is_open()){
        cout << "File open error." << endl;
        return -1;
    }

    const int bufSize = 1000;
    char str[bufSize];
    while (!text.eof()) {
        text.read(str, bufSize - 1);
        str[text.gcount()] = 0;
        cout << str << endl;
        cout << "Scroll? (y/n): ";
        char scroll;
        do
            cin >> scroll;
        while(scroll != 'y' && scroll != 'n');
        if(scroll == 'n') break;
    }
    text.close();
    return 0;
}
